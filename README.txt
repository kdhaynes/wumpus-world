############################################################################
Logic Inference in the Wumpus World
############################################################################

This project has two directories:
- docs: Contains the project description
- python: Contains the python program and input files.
          Alternate input files are provided in the alt_tests subfolder.


The program wworld_logic.py determines if a query is entailed in the
Wumpus World.  The programs uses python 3.3 with z3, developed and tested
on a Macbook.

I obtained python from the Anaconda distribution, and I downloaded z3 using:
>pip install z3-solver.

To run the program, type:
>python3 wworld_logic.py

The program requires two input files.  The first, beliefs.txt, provides the
current knowledge base.  The second, query.txt, provides a query that
needs to be decided based on the knowledge base.  Both files need to be
provided in the current working directory.

The output to this program is a single statement.  If the query can be
entailed into the knowledge base, the program prints 'entails'.  Otherwise,
the program prints 'does not entail'.

No time limit is currently set on the program, as it was unclear how
to stop the z3 routines mid-run since those use established libraries
and routines.  However, time limitation is not expected to be a problem,
as all tests finished in ample time (<< 1 minute).  The time taken for
the program can be saved and printed out by uncommenting the time-related
lines at the beginning and end.

I did the program allowing more than one Wumpus and more than one Gold.
I also then did the query entailment allowing only one Wumpus and
only one Gold.  I found this to be more challenging in providing the
constraints in the proper format, finally using the z3.AtMost function.
Even though this was not required for the homework, I left this
capability in the program.  To restrict the entailment to only a
single Wumpus, at the top of the program set the singleWumpus flag to True.
Similarly, to restrict the entailment to only a single Gold, at the top
of the program set the singleGold flag to True.


############################################################################
Katherine Haynes
CS440 Assignment 4
2019/04/10