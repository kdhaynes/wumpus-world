#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat Apr  6 10:12:35 2019

@author: kdhaynes
"""

#from z3 import *
import z3
#import time==================

# Set filenames

#======================
directory='./'
filenameb=directory + 'beliefs.txt'
filenameq=directory + 'query.txt'

noffset=6
singleWumpus = False
singleGold = False

# Start timer:
#t0=time.time()

# Initialize the solver:
s = z3.Solver()  

# Read in size of board:
with open(filenameb) as f:
    nboard = int(f.readline().rstrip())

# Read in query file:
with open(filenameq) as f:
     line = f.readline().rstrip()
icenter = int(line.split()[0])
jcenter = int(line.split()[1])
pquery = line.split()[2]

# Create matrices around query
if (nboard+2 < noffset*2+1):
    istart = 0
    jstart = 0
    nmatrix = nboard+2
else:
   istart = icenter - noffset
   if (istart < 0):
       istart = 0
   if (icenter + noffset > nboard):
       istart -= (icenter+noffset-nboard-1)

   jstart = jcenter - noffset
   if (jstart < 0):
       jstart = 0
   if (jcenter + noffset > nboard):
       jstart -= (jcenter+noffset-nboard-1)
   nmatrix = noffset*2+1

istack = [i + istart for i in range(nmatrix)]
jstack = [j + jstart for j in range(nmatrix)]
Cells = [(i,j) for j in jstack for i in istack]
Breeze =  [ [ z3.Bool("Breeze_%s_%s" % (i,j)) 
               for j in jstack ] for i in istack ]
Pit =     [ [ z3.Bool("Pit_%s_%s" % (i,j)) 
               for j in jstack ] for i in istack ]
Stench =  [ [ z3.Bool("Stench_%s_%s" % (i,j)) 
               for j in jstack ] for i in istack ]
Wumpus =  [ [ z3.Bool("Wumpus_%s_%s" % (i,j)) 
               for j in jstack ] for i in istack ] 
WumpusOffBC = False
Glitter = [ [ z3.Bool("Glitter_%s_%s" % (i,j))
               for j in jstack ] for i in istack ]
Gold =    [ [ z3.Bool("Gold_%s_%s" % (i,j)) 
               for j in jstack ] for i in istack ]
GoldOffBC = False

# Read in input file:
with open(filenameb) as f:
    nboard = int(f.readline().rstrip())
    line = f.readline().rstrip()
    
    while line:
        bi = int(line.split()[0])
        bj = int(line.split()[1])
        percept = line.split()[2]
        
        #....Add in belief constraints
        if ((bi,bj) in Cells):
           myi = bi-istart
           myj = bj-jstart
           if (percept == 'Breeze'):
               s.add(Breeze[myi][myj] == True)
           elif (percept == 'NoBreeze'):
               s.add(Breeze[myi][myj] == False)
           elif (percept == 'Pit'):
               s.add(Pit[myi][myj] == True)
           elif (percept == 'NoPit'):
               s.add(Pit[myi][myj] == False)
           elif (percept == 'Stench'):
               s.add(Stench[myi][myj] == True)
           elif (percept == 'NoStench'):
               s.add(Stench[myi][myj] == False)
           elif (percept == 'Wumpus'):
               s.add(Wumpus[myi][myj] == True)
           elif (percept == 'NoWumpus'):
               s.add(Wumpus[myi][myj] == False)
           elif (percept == 'Glitter'):
               s.add(Glitter[myi][myj] == True)
           elif (percept == 'NoGlitter'):
               s.add(Glitter[myi][myj] == False)
           elif (percept == 'Gold'):
               s.add(Gold[myi][myj] == True)
           elif (percept == 'NoGold'):
               s.add(Gold[myi][myj] == False)

        else:
           if (singleWumpus and 
               percept == 'Stench' or percept == 'Wumpus'):
               WumpusOffBC = True
               s.add(WumpusOffBC)
           elif (singleGold and
                 percept == 'Glitter' or percept == 'Gold'):
               GoldOffBC = True
               s.add(GoldOffBC)
               
        #....Read next line
        line = f.readline().rstrip()

# Add all wall constraints
if (istart == 0):
   s.add([Pit[0][j] == False for j in range(nmatrix)])
   s.add([Wumpus[0][j] == False for j in range(nmatrix)])
if (nmatrix-1 > nboard):
   s.add([Pit[nmatrix-1][j] == False for j in range(nmatrix)])
   s.add([Wumpus[nmatrix-1][j] == False for j in range(nmatrix)])
if (jstart == 0):
   s.add([Pit[i][0] == False for i in range(nmatrix)])
   s.add([Wumpus[i][0] == False for i in range(nmatrix)])
if (nmatrix-1 > nboard):
   s.add([Pit[i][nmatrix-1] == False for i in range(nmatrix)])
   s.add([Wumpus[i][nmatrix-1] == False for i in range(nmatrix)])

# Add Game Rule Constraints
#....Breezes and Pits
Pit_c = [z3.Implies(Pit[i][j],
                     z3.And(Breeze[i+1][j],
                            z3.And(Breeze[i-1][j], 
                                   z3.And(Breeze[i][j+1],
                                          Breeze[i][j-1]))))
            for i in range(1,nmatrix-1) for j in range(1,nmatrix-1)]
Breeze_c = [z3.Implies(Breeze[i][j],
                       z3.Or(Pit[i+1][j],
                             z3.Or(Pit[i-1][j],
                                   z3.Or(Pit[i][j+1], Pit[i][j-1]))))
            for i in range(1,nmatrix-1) for j in range(1,nmatrix-1)]
NoBreeze_c = [z3.Implies(z3.Not(Breeze[i][j]),
                         z3.And(z3.Not(Pit[i+1][j]),
                                z3.And(z3.Not(Pit[i-1][j]),
                                   z3.And(z3.Not(Pit[i][j+1]), 
                                          z3.Not(Pit[i][j-1])))))
            for i in range(1,nmatrix-1) for j in range(1,nmatrix-1)]
s.add(Pit_c + Breeze_c + NoBreeze_c) 

#...Stenches and Wumpus
if (singleWumpus):
   WumpusOffBC_c = [z3.Implies(WumpusOffBC,
                               z3.And(z3.Not(Stench[i][j]),
                                      z3.Not(Wumpus[i][j])))
                    for i in range(nmatrix) for j in range(nmatrix)]
   s.add(WumpusOffBC_c)

   WumpusOne_c = z3.AtMost([Wumpus[i][j] for i in range(nmatrix) 
                            for j in range(nmatrix)] + [1])
   s.add(WumpusOne_c)
    
Wumpus_c = [z3.Implies(Wumpus[i][j],
                     z3.And(Stench[i+1][j],
                            z3.And(Stench[i-1][j], 
                                   z3.And(Stench[i][j+1],
                                          Stench[i][j-1]))))
            for i in range(1,nmatrix-1) for j in range(1,nmatrix-1)]
Stench_c = [z3.Implies(Stench[i][j],
                 z3.Or(Wumpus[i][j],
                       z3.Or(Wumpus[i+1][j],
                             z3.Or(Wumpus[i-1][j],
                                   z3.Or(Wumpus[i][j+1], 
                                         Wumpus[i][j-1])))))
            for i in range(1,nmatrix-1) for j in range(1,nmatrix-1)]
NoStench_c = [z3.Implies(z3.Not(Stench[i][j]),
                         z3.And(z3.Not(Wumpus[i][j]),
                            z3.And(z3.Not(Wumpus[i+1][j]),
                                z3.And(z3.Not(Wumpus[i-1][j]),
                                   z3.And(z3.Not(Wumpus[i][j+1]), 
                                          z3.Not(Wumpus[i][j-1]))))))
            for i in range(1,nmatrix-1) for j in range(1,nmatrix-1)]
s.add(Wumpus_c + Stench_c + NoStench_c) 

#....Glitter and Gold
if (singleGold):
    GoldOffBC_c = [z3.Implies(GoldOffBC,
                          z3.And(z3.Not(Glitter[i][j]),
                                 z3.Not(Gold[i][j])))
             for i in range(nmatrix) for j in range(nmatrix)]
    s.add(GoldOffBC_c)
    
    GoldOne_c = z3.AtMost([Gold[i][j] for i in range(nmatrix) 
                            for j in range(nmatrix)] + [1])
    s.add(GoldOne_c)
    GlitterOne_c = z3.AtMost([Glitter[i][j] for i in range(nmatrix)
                             for j in range(nmatrix)] + [1])
    s.add(GlitterOne_c)
    
Glitter_c   = [z3.Implies(Glitter[i][j],Gold[i][j])
               for i in range(nmatrix) for j in range(nmatrix)]
NoGlitter_c = [z3.Implies(z3.Not(Glitter[i][j]),z3.Not(Gold[i][j]))
               for i in range(nmatrix) for j in range(nmatrix)]
s.add(Glitter_c + NoGlitter_c)

# Add query contraint
#....negating the query for proof of entailment
if (pquery == 'Breeze'):
    s.add(Breeze[icenter-istart][jcenter-jstart] == False)
if (pquery == 'NoBreeze'):
    s.add(Breeze[icenter-istart][jcenter-jstart] == True)
if (pquery == 'Pit'):      
    s.add(Pit[icenter-istart][jcenter-jstart] == False)
if (pquery == 'NoPit'):
    s.add(Pit[icenter-istart][jcenter-jstart] == True)
if (pquery == 'Stench'):      
    s.add(Stench[icenter-istart][jcenter-jstart] == False)
if (pquery == 'NoStench'):
    s.add(Stench[icenter-istart][jcenter-jstart] == True)
if (pquery == 'Wumpus'):      
    s.add(Wumpus[icenter-istart][jcenter-jstart] == False)
if (pquery == 'NoWumpus'):
    s.add(Wumpus[icenter-istart][jcenter-jstart] == True)
if (pquery == 'Glitter'):      
    s.add(Glitter[icenter-istart][jcenter-jstart] == False)
if (pquery == 'NoGlitter'):
    s.add(Glitter[icenter-istart][jcenter-jstart] == True)
if (pquery == 'Gold'):      
    s.add(Gold[icenter-istart][jcenter-jstart] == False)
if (pquery == 'NoGold'):
    s.add(Gold[icenter-istart][jcenter-jstart] == True)

# Print the results
#...true means the negative query could be found, 
#...so the query could not be entailed
if s.check() == z3.sat:
    print("does not entail")
    #m = s.model()
    #print(m)

#...false means the negative query could not be found,
#...so the original query can be entailed because
#...it must hold for all possible worlds
else:
    print ("entails")
    #m = s.model()
    #print(m)

# Print out time information
#timetaken = time.time() - t0
#print ()
#print ("Time Taken (s): ",'{:.5f}'.format(timetaken))

